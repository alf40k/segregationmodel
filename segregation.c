// file: segregation.c
// date: Sat 9/21/15
//

#define _BSD_SOURCE
#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/time.h>
#include <string.h>
#include <sys/select.h>
#include <ctype.h>

#include "display.h"
#include "segregation.h"

#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif /* __STDC_VERSION__ */

/**////////////////////////////////////////////////////////////////////////
// @file segregation.c
// @author all3187 : Arthur Lunn
// @date 21 Sep 2015
// @ Main file simulating Thomas Schelling's Model of Segregation. 
// 
// This program is designed to simulate cycles for a segregation model.
// It uses Thomas Schelling's Model to simulate cycles. 
//
//
// The simulation will use the '+' as the rich characters and
// will represent the poor characters as '-'. Any vacant locations will
// be represented with the '.' character. The program must take in 4 command
// line arguments with an optional 5th. The order of the command line 
// arguments must be size satisfaction vacancy proportion. The command
// line arguments specify the following:
//
//      - size Species the height and width. Must be 5-39.
//      - satisfaction The satisfaction threshold as a percentage.
//      - vacancy The percentage of the starting space to start vacant
//      - proportion The percentage of occupied spaces that start rich.
//      - -pN (Optional) If selected runs in print mode where N 
//                       determines the number of cycles to simulate.
//
// @param argc The number of arguments supplised; should be 4 - 6.
// @param argv The arguments options are:
//      - size Speciies the height and width. Must be 5-39.
//      - satisfaction The satisfaction threshold as a percentage.
//      - vacancy The percentage of the starting space to start vacant
//      - proportion The percentage of occupied spaces that start rich.
//      - -pN (Optional) If selected runs in print mode where N 
//                       determines the number of cycles to simulate.
// @return Returns 0 if succesful.
/////////////////////////////////////////////////////////////////////////*/    
int main(int argc, char **argv){
    int size, satisfaction, vacancy, proportion,  // The CL arguments
        printMode, isRunning, agentsMoved,         // Necesary flags
        cycle, clOption, printCycles, optionArg;
    char option;
    float segregationMeasure;
    cycle = 0; 
    printMode = 0;
    isRunning = 1;
    segregationMeasure = 0;
    agentsMoved = 0;
    static char usage[] = "usage: segregation [-pN] size satisfaction vacancy proportion";

    srand( 41 );
    // Process -pN if supplied
    while( ( clOption = getopt( argc, argv, "p:" ) ) != -1 ){
        switch(clOption){
            case 'p':
                // This needs to be checked or handled better
                printCycles = atol(optarg);
                printMode = 1;
                break;
            case '?':
                if ( optopt == 'p'){
                    fprintf(stderr, "-pN needs an argument\n%s\n", usage);
                    return -1;
                }
                else{
                    fprintf(stderr, "%s\n", usage);
                    return -1;
                }
                break;
        }
    }
    // Check for proper number of command line arguments
    if( argc - optind != 4 ){
           fprintf(stderr, "%s\n", usage);
           return -1;     
    }
    // Process remaining command line arguments
    for( int i = 0; i < 4; i++){
        switch (i) {
            case  0 :
                if( ( size = atol( argv[i+optind] ) )  == 0 ||
                    ( size < 5 || size > 39 ) ){
                        fprintf(stderr, "size ({vvv}) must be an integer in [5...39]\n%s\n", usage);
                        return -1;                         
                }
                break;               
            case  1 :
                if( ( ( satisfaction = atol( argv[i+optind] ) ) == 0 ) ||
                    ( satisfaction < 1 || satisfaction > 99 ) ){
                        fprintf(stderr, "satisfaction ({vvv}) must be an integer in [1...99]\n%s\n", usage);    
                        return -1;                     
                }
                break;
            case  2 :
                if( ( ( vacancy = atol( argv[i+optind] ) ) == 0 ) ||
                    ( vacancy < 1 || vacancy > 99 ) ){
                        fprintf(stderr, "vacancy ({vvv}) must be an integer in [1...99]\n%s\n", usage);    
                        return -1;                     
                }
                break;
            case  3 :
                if( ( ( proportion = atol( argv[i+optind] ) ) == 0 ) ||
                    ( proportion < 1 || proportion > 99 ) ){
                        fprintf(stderr, "proportion ({vvv}) must be an integer in [1...99]\n%s\n", usage);    
                        return -1;                     
                }
                break;
            
        }
    }
    // Setup initial model
    char model[size][size];
    setupAgents(size, model, vacancy, proportion);  
    segregationMeasure = getSegregationMeasure(size, model);
    // Handles print mode with a simple for loop
    if( printMode == 1 ){
        for( int i = 0; i<printCycles+1; i++){
            displayModelPrt(size, model);
            displayStats(cycle, agentsMoved, segregationMeasure);
            segregationMeasure = getSegregationMeasure(size, model);
            displaySettings(size, satisfaction, vacancy, proportion);
            agentsMoved = runCycle(size, model, satisfaction);
            cycle++;
        }
    }
    // Otherwise initializes the board in interactive mode
    else{
        clear();
        displayModelInt(size, model);
        displayStats(cycle, agentsMoved, segregationMeasure);
        displayCurrentState(isRunning);
        displayOptions();
        set_cur_pos(size+8, 1);
        put('>');
        // Gets the user supplied options and deals with them appropriatly
        while(1){
            if( getOption() != 0){
                option = getchar();
                switch(option){
                    // Toggle run state
                    case '/':
                        isRunning ^= 1;
                        break;
                    // Stop model and step one cycle           
                    case '.':
                        agentsMoved = runCycle(size, model, satisfaction);
                        cycle++;
                        isRunning = 0;
                        break;
                    // Reset model
                    case 'r':
                        setupAgents(size, model, vacancy, proportion);
                        agentsMoved = 0;
                        segregationMeasure = getSegregationMeasure(size, model);
                        clear();    
                        cycle = 0;
                        isRunning = 0;
                        break;
                    // Set satisfaction
                    case 's':
                        if ( scanf("%d", &optionArg) == 0  || ( optionArg < 1 || optionArg > 99 ) ){
                            printf("\nsatisfaction ({vvv}) must be an integer in [1...99]      ");
                        }
                        else{
                            satisfaction = optionArg;
                            clear();
                        }
                        break;
                    // Set vacancy
                    case 'v':
                        if ( scanf("%d", &optionArg) == 0  || ( optionArg < 1 || optionArg > 99 ) ){
                            printf("\nvacancy ({vvv}) must be an integer in [1...99]      ");
                        }
                        else{
                            vacancy = optionArg;
                            setupAgents(size, model, vacancy, proportion);
                            agentsMoved = 0;
                            segregationMeasure = getSegregationMeasure(size, model);
                            clear();
                        }
                        break;
                    // Set proportion
                    case 'p':
                        if ( scanf("%d", &optionArg) == 0  || ( optionArg < 1 || optionArg > 99 ) ){
                            printf("\nproportion ({vvv}) must be an integer in [1...99]      ");
                        }
                        else{
                            proportion = optionArg;
                            setupAgents(size, model, vacancy, proportion);
                            agentsMoved = 0;
                            segregationMeasure = getSegregationMeasure(size, model);                        
                            clear();
                        }
                        break;
                    // Display info        
                    case 'i':
                        displaySettings(size, satisfaction, vacancy, proportion);
                        break;
                    // H/help was not included as it provides no true functionality.
                    // The information that H would display is already ALWAYS
                    // being displayed on screen. This just creates clutter.
                    case 'q':
                        return 0;
                        break;
                    default:
                        break;
                }
                // This is a weak work around
                // Code should ideally be reworked not to use scanf
                while((option = getchar()) != '\n' && option != EOF);
            }
            // If the program is running run a "cycle" of the ode
            if(isRunning == 1){
                agentsMoved = runCycle(size, model, satisfaction);
                cycle++;

            }
            // No matter what re-display the model and relevent information
            displayModelInt(size, model);
            segregationMeasure = getSegregationMeasure(size, model);
            displayStats(cycle, agentsMoved, segregationMeasure);
            displayCurrentState(isRunning);
            displayOptions();
            set_cur_pos(size+8, 1);
            put('>');
        }
    }
}


/*////////////////////////////////////////////////////////////////////////
// Setup an initial agent.
//
// @param size The size of the model.
// @param model The current model.
// @param pop The total number of characters to place on the model.
// @param c The character to place
////////////////////////////////////////////////////////////////////////*/
void setupAgents(int size, char model[][size], int vacancy, int proportion){
    int currentRich, currentPoor, totalSpaces, totalPoor,
        totalVacancy, totalRich;
    int row, col;
    currentRich = 0;
    currentPoor = 0;
    // Calculate necesary values for setting up the agents
    totalSpaces = size * size;
    totalVacancy = ( totalSpaces * vacancy ) / 100;
    totalRich = ( (totalSpaces - totalVacancy ) * proportion )
                  / 100 ;
    totalPoor = (totalSpaces - totalVacancy - totalRich);
    // Place all of the agents
    for( row = 0; row < size; row++){
        for( col = 0; col < size; col++){
            if ( currentRich < totalRich ){
                currentRich++;
                model[row][col] = '+';
            }
            else if( currentPoor < totalPoor ){
                currentPoor++;
                model[row][col] = '-';
            }
            else{
                model[row][col] = '.';
            }
        }
    }
    // Shuffle the model
    shuffle2dAray(size, model);
}

/*////////////////////////////////////////////////////////////////////////
// Shuffles the given model.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void shuffle2dAray(int size, char model[][size]){
    int row, col, rowR, colR;
    char c;
    for( row = 0; row < size; row++){
        for( col = 0; col < size; col++){
                rowR = random();
                rowR %= size;
                colR = random();
                colR %= size;
                c = model[row][col];
                model[row][col] = model[rowR][colR];
                model[rowR][colR] = c;
        }
    }    
}

/*////////////////////////////////////////////////////////////////////////
// While named "moveAgent" this method actually serves to copy an agent
// into another location. The calling program must handle "clearing"
// the location that has been moved from
//
// @param size The size of the model.
// @param model The current model.
// @return 1 if more succesful 0 otherwise.
////////////////////////////////////////////////////////////////////////*/
int moveAgent(int size, char modelTemp[][size], char model[][size], char c){
    int row, col;
    for( row = 0; row < size; row++ ){
        for( col = 0; col < size; col++ ){
            if( model[row][col] == '.' && modelTemp[row][col] == '.' ){
                modelTemp[row][col] = c;
                return 1;
            }
        }
    }
    return 0;
}


/**///////////////////////////////////////////////////////////////////////
// Simple function to iterate through the model and display it
// to standard output for interactive mode.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void displayModelInt(int size, char model[][size]){
    int row, col;
    // iterate through and display all locations
    for( row = 0; row < size; row++ ){
        for( col = 0; col < size; col++ ){
            //Set the cursor adjusting for display window discrepency
            set_cur_pos( ( row + 1 ) , ( col ) );
            put( model[row][col] );
        }
        // advance the cursor 1 line
        puts( " " );
    }
}

/**///////////////////////////////////////////////////////////////////////
// Simple function to iterate through the model and display it
// to standard output for print mode.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void displayModelPrt(int size, char model[][size]){
    int row, col;
    // iterate through and display all cells
    for( row = 0; row < size; row++ ){
        for( col = 0; col < size; col++ ){
            //Set the cursor adjusting for display window discrepency
            printf("%c", model[row][col]);
        }
        // advance the cursor 1 line
        puts( " " );
    }
}

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move statistics
// and options for "continuous play" mode.
//
// @param cycle The current cycle
// @param agentsMoved Number of agents moved this cycle
// @param segregationMeasure The model's current segregation measure
////////////////////////////////////////////////////////////////////////*/
void displayStats(int cycle, int agentsMoved, float segregationMeasure){
    printf("cycle: %d    ", cycle);
    printf("\nMoves this cycle: %d    ", agentsMoved);
    printf("\nsegregation measure: %f", segregationMeasure);
}

/**///////////////////////////////////////////////////////////////////////
// Simple function to display the current run status of the program.
//
// @param isRunning flag to determine if the model is running
////////////////////////////////////////////////////////////////////////*/
void displayCurrentState(int isRunning){
    static char* state;
    if (isRunning == 1)
        state = "running    ";
    else
        state = "paused    ";
    printf("\n\nstate: %s\n", state);
}

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move options.
////////////////////////////////////////////////////////////////////////*/
void displayOptions(){
    static char options[] = "(/)stop/go, (.)step (r)eset, (s)at N, (v)ac N, (p)rop N, (q)uit, (i)nfo,\n(h)elp";
    printf("%s\n", options);

}

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move statistics.
////////////////////////////////////////////////////////////////////////*/
void displaySettings(int size, int satisfaction, int vacancy, int proportion){
    printf("\nsize %d, satisfaction 0.%d, vacancy 0.%d, proportion 0.%d\n",
           size, satisfaction,vacancy,proportion);

}
/**///////////////////////////////////////////////////////////////////////
// Simple function to get user option. Will timeout after 2 sec.
//
// @return User option.
////////////////////////////////////////////////////////////////////////*/
int getOption(){
    struct timeval *timeout = malloc(sizeof *timeout);
    timeout->tv_usec = 0;
    timeout->tv_sec = 2;
    fd_set readSet, writeSet, exceptSet;
    FD_ZERO(&readSet);
    FD_ZERO(&writeSet);
    FD_ZERO(&exceptSet);
    FD_SET(fileno(stdin), &readSet);
    return select(fileno(stdin)+1, &readSet, &writeSet, &exceptSet, timeout);
}


/**///////////////////////////////////////////////////////////////////////
// The runCycle function will evaluate the model for a single "cycle".
// Any agents who are unhappy will move to another location. An agent is
// determined to be "unhappy" if the percent of like agents is equal to or
// greater than the satisfaction threshold
//
// @param size The size of the model.
// @param model The current model.
// @return The number of agents moved
////////////////////////////////////////////////////////////////////////*/
int runCycle(int size, char model[][size], int satisfaction){
    int row, col, currentSatisfaction, agentsMoved;
    agentsMoved = 0;
    // Sets the sizeOfmodel to the total bit size of the model array
    size_t sizeOfmodel = size*size*sizeof(char);
    // Make a temp array to hold the changes in and copy model into it
    char modelTemp[size][size];
    memcpy(modelTemp, model, sizeOfmodel);
    // Go through all cells and evaluate them based on rules
    for( row = 0; row < size; row++ ){
        for( col = 0; col < size; col++ ){
            if( model[row][col] == '+' || model[row][col] == '-' ){
                currentSatisfaction = findSatisfaction( size, model, row, col,
                                                        model[row][col] );
                if (currentSatisfaction < satisfaction){
                    if( moveAgent(size, modelTemp,
                                  model, model[row][col] ) == 1){
                        agentsMoved++;
                        modelTemp[row][col] = '.';
                    }
                }
            }
        }
    }
    // copy the changes into the model array
    memcpy( model, modelTemp, sizeOfmodel );
    return agentsMoved;
}

/**///////////////////////////////////////////////////////////////////////
// Simple function to get the total segregation measure.
//
// @param size The size of the model.
// @param model The current model.
// @return The current segregation measure.
////////////////////////////////////////////////////////////////////////*/
float getSegregationMeasure(int size, char model[][size]){
    int totalSatisfaction, totalAgents, row, col, currentSatisfaction;
    totalAgents = 0;
    totalSatisfaction = 0;
    for( row = 0; row < size; row++ ){
        for( col = 0; col < size; col++ ){
            if( model[row][col] == '+' || model[row][col] == '-' ){
                currentSatisfaction = findSatisfaction( size, model, row, col,
                                                        model[row][col] );

                totalSatisfaction+=currentSatisfaction;
                totalAgents++;
            }
        }
    } 
    return totalSatisfaction/(float)(totalAgents*100);
}

/**///////////////////////////////////////////////////////////////////////
// Function designed to count the total number of neighbors given a row
// and col in the model. The function will count all adjacent cells
// that host a character and return the percentage "like" as an integer.
// 
// @param size The size of the model.
// @param model The current model.
// @param row The row index to evaluate.
// @param col The col index to evaluate.
// @return The total number of neighboring cells with '*' hosted in them.
////////////////////////////////////////////////////////////////////////*/
int findSatisfaction(int size, char model[][size], int row, int col, char c){
    int neighbors = 0;
    int likeNeighbors = 0;
    // Check all neighbors
    if( ( row != 0 && col != 0 ) && model[row - 1][col - 1] != '.' ){
        ++neighbors;
        if ( model[row - 1][col - 1] == c )
            likeNeighbors++;
    }
    if( row != 0 && model[row - 1][col] != '.' ){
        ++neighbors;
        if ( model[row - 1][col] == c )
            likeNeighbors++;
    }
    if( ( row != 0 && col != ( size - 1 ) ) && model[row - 1][col + 1] != '.' ){
        ++neighbors;
        if ( model[row - 1][col + 1] == c )
            likeNeighbors++;
    }     
    if( col != 0 && model[row][col - 1] != '.'){
        ++neighbors;
        if ( model[row][col - 1] == c )
            likeNeighbors++;
    }  
    if( col != ( size - 1 ) && model[row][col + 1] != '.' ){
        ++neighbors;
        if ( model[row][col + 1] == c )
            likeNeighbors++;
    } 
    if( ( row != ( size - 1 ) && col != 0 ) && model[row + 1][col - 1] != '.' ){
        ++neighbors;
        if ( model[row + 1][col - 1] == c )
            likeNeighbors++;
    }  
    if( row != ( size - 1 ) && model[row + 1][col] != '.' ){
        ++neighbors;
        if ( model[row + 1][col] == c )
            likeNeighbors++;
    }
    if( ( row != ( size - 1 ) && col != ( size - 1 ) ) && model[row + 1][col + 1] != '.' ){
        ++neighbors;
        if ( model[row + 1][col + 1] == c )
            likeNeighbors++;
    } 
    if ( neighbors == 0 ){
        return 100;
    }
    // Calculate and return the segregation measure
    return  ( 100 * likeNeighbors ) / ( neighbors );
}
