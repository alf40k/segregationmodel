# README #

### Summary ###

This program is designed to represent  Thomas Schelling's Models of Segregation. It uses basic C structures to get the job done. Currently memory is not handled in a safe way and program needs major revisions.

Program usage is as follows

    usage: segregation [-pN] size satisfaction vacancy proportion

Currently the program expected correct usage and may fail if provided with incorrect usage. This is something that needs to be remedied.


### Development ###

This program was written as an introduction to the C language. At some point it may be expanded to include other 0 player games and models in C. At the least the program should be expanded to handle memory and errors in a more robust way.

### Contact ###

Please contact alf40k@gmail.com or ArthurLLunn@gmail.com with any questions