#ifndef SEGREGATION_H
#define SEGREGATION_H

/*////////////////////////////////////////////////////////////////////////
// Setup an initial agent.
//
// @param size The size of the model.
// @param model The current model.
// @param pop The total number of characters to place on the model.
// @param c The character to place
////////////////////////////////////////////////////////////////////////*/
void setupAgents(int size, char model[][size], int vacancy, int proportion);

/*////////////////////////////////////////////////////////////////////////
// Shuffles the given model.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void shuffle2dAray(int size, char model[][size]);

/*////////////////////////////////////////////////////////////////////////
// While named "moveAgent" this method actually serves to copy an agent
// into another location. The calling program must handle "clearing"
// the location that has been moved from
//
// @param size The size of the model.
// @param model The current model.
// @return 1 if more succesful 0 otherwise.
////////////////////////////////////////////////////////////////////////*/
int moveAgent(int size, char modelTemp[][size], char model[][size], char c);

/**///////////////////////////////////////////////////////////////////////
// Simple function to iterate through the model and display it
// to standard output for interactive mode.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void displayModelInt(int size, char model[][size]);

/**///////////////////////////////////////////////////////////////////////
// Simple function to iterate through the model and display it
// to standard output for print mode.
//
// @param size The size of the model.
// @param model The current model.
////////////////////////////////////////////////////////////////////////*/
void displayModelPrt(int size, char model[][size]);

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move statistics
// and options for "continuous play" mode.
//
// @param cycle The current cycle
// @param agentsMoved Number of agents moved this cycle
// @param segregationMeasure The model's current segregation measure
////////////////////////////////////////////////////////////////////////*/
void displayStats(int cycle, int agentsMoved, float segregationMeasure);

/**///////////////////////////////////////////////////////////////////////
// Simple function to display the current run status of the program.
//
// @param isRunning flag to determine if the model is running
////////////////////////////////////////////////////////////////////////*/
void displayCurrentState(int isRunning);

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move options.
//////////////////////////////////////////////////////////////
void displayOptions();

/**///////////////////////////////////////////////////////////////////////
// Simple function to get user option. Will timeout after 2 sec.
//
// @return User option.
////////////////////////////////////////////////////////////////////////*/
int getOption();

/**///////////////////////////////////////////////////////////////////////
// Simple function to display all of the relevent move statistics.
////////////////////////////////////////////////////////////////////////*/
void displaySettings(int size, int satisfaction, int vacancy, int proportion);

/**///////////////////////////////////////////////////////////////////////
// The runCycle function will evaluate the model for a single "cycle".
// Any agents who are unhappy will move to another location. An agent is
// determined to be "unhappy" if the percent of like agents is equal to or
// greater than the satisfaction threshold
//
// @param size The size of the model.
// @param model The current model.
// @return The number of agents moved
////////////////////////////////////////////////////////////////////////*/
int runCycle(int size, char model[][size], int satisfaction);

/**///////////////////////////////////////////////////////////////////////
// Simple function to get the total segregation measure.
//
// @param size The size of the model.
// @param model The current model.
// @return The current segregation measure.
////////////////////////////////////////////////////////////////////////*/
float getSegregationMeasure(int size, char model[][size]);

/**///////////////////////////////////////////////////////////////////////
// Function designed to count the total number of neighbors given a row
// and col in the model. The function will count all adjacent cells
// that host a character and return the percentage "like" as an integer.
// 
// @param size The size of the model.
// @param model The current model.
// @param row The row index to evaluate.
// @param col The col index to evaluate.
// @return The total number of neighboring cells with '*' hosted in them.
////////////////////////////////////////////////////////////////////////*/
int findSatisfaction(int size, char model[][size], int row, int col, char c);


#endif